const express = require('express');

function list(req, res, next) {
    res.send('Lista de usuarios del sistema');
}

function index(req, res, next) {
    res.send(`Usuario del sistema con id = ${req.params.id}` );
}

function create(req, res, next) {
    const name = req.body.name;
    const lastname = req.body.lastname;
    res.send(`Crear un usuario con nombre ${name} y apellido ${lastname}`);
}

function replace(req, res, next) {
    res.send(`Reemplazo un usuario con id ${req.params.id} por otro`);
}

function edit(req, res, next) {
    res.send(`Edito un usuario con id ${req.params.id}`);
}

function destroy(req, res, next) {
    res.send(`Elimino un usuario con id ${req.params.id}`);
}

module.exports = {
    list,
    index,
    create,
    replace,
    edit,
    destroy
}
